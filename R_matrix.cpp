#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main(){
    srand(time(0));
    
    int n, m;
    cout<<"Введіть розмір матриці..."<<endl;
    cout<<"Кількість рядків:";
    cin>>n;
    cout<<"Кількість стовпців:";
    cin>>m;
    int mas[n][m];
    for (int i=0;i<n;i++){
        for (int j=0;j<m;j++){
            mas[i][j]=rand()%32767 + 1;
        }
    }
    for (int i=0;i<n;i++){
        for (int j=0;j<m;j++){
            cout<<mas[i][j]<<"\t";
        }
        cout<<"\n";
    }

    return 0;
}
